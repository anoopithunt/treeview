<!DOCTYPE html> 

<html> 

<head> 

    <title>Treeview Example</title> 

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" target="_blank" rel="nofollow"  /> 

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" target="_blank" rel="nofollow"  rel="stylesheet"> 
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 

    <link href="/css/treeview.css" target="_blank" rel="nofollow"  rel="stylesheet"> 

</head> 

<body> 

    <div class="container">      

        <div class="panel panel-primary"> 

            <div class="panel-heading">Manage TreeView</div> 

                <div class="panel-body"> 

                    <div class="row"> 

                        <div class="col-md-6"> 

                            <h3>Tree entry List without ajax </h3> 

                            <ul id="tree1"> 

                                @foreach($treeEntries as $value) 

                                    <li> 

                                        {{ $value->entry->name }} 

                                        @if(count($value->childs)) 

                                            @include('manageChild',['childs' => $value->childs]) 

                                        @endif 

                                    </li> 

                                @endforeach 

                            </ul> 

                        </div> 


                    </div> 

                </div> 

            </div> 

        </div> 

    </div> 

    <script src="/js/treeview.js"></script> 

</body> 

</html> 