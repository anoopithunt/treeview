<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreeEntryLangM extends Model
{
    protected $table='tree_entry_lang';
    protected $fillable = [
       'entry_id', 'lang','name'
    ];
}
