<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreeEntryM extends Model
{
    protected $table='tree_entry';
    protected $fillable = [
       'entry_id', 'parent_entry_id'
    ];

    public function entry(){
         return $this->hasOne('App\TreeEntryLangM','entry_id','entry_id');
    }

    public function parentEntry(){
         return $this->hasOne('App\TreeEntryLangM','entry_id','parent_entry_id');
    }

    public function childs(){
        return $this->hasMany('App\TreeEntryM','parent_entry_id','entry_id');
    }
}
